;;; init-gui.el --- .Emacs Configuration -*- lexical-binding: t -*-
;;; Commentary:
;;

;;; Code:
;;-----------------------
;; Remove some GUI stuff
;;-----------------------
(setq use-file-dialog nil)
(setq use-dialog-box nil)
(setq inhibit-startup-screen t)
(setq inhibit-startup-echo-area-message t)
(tool-bar-mode 0)
(set-scroll-bar-mode nil)
(menu-bar-mode 0)
(setq make-backup-files nil) ; stop creating backup~ files
(setq auto-save-default nil) ; stop creating #autosave# files
;; Suppressing ad-handle-definition Warnings in Emacs
(setq ad-redefinition-action 'accept)

;;------------------------------
;; Core settings | UTF-8 please
;;------------------------------
(set-charset-priority 'unicode)
(set-language-environment "UTF-8")
(set-terminal-coding-system  'utf-8)   ; pretty
(set-keyboard-coding-system  'utf-8)   ; pretty
(set-selection-coding-system 'utf-8)   ; please
(prefer-coding-system        'utf-8)   ; with sugar on top

;;----------------------------------------------------------------------------
;; Editor configuration
;;----------------------------------------------------------------------------
(setq indicate-empty-lines t)

(let ((no-border '(internal-border-width . 0)))
  (add-to-list 'default-frame-alist no-border)
  (add-to-list 'initial-frame-alist no-border))

(setq frame-title-format
      '((:eval (if (buffer-file-name)
                   (abbreviate-file-name (buffer-file-name))
                 "%b"))))
;; Non-zero values for `line-spacing' can mess up ansi-term and co,
;; so we zero it explicitly in those cases.
(add-hook 'term-mode-hook
	  (lambda ()
	    (setq line-spacing 0)))

;; Font theme (Monospace, or DejaVu Sans Mono if Monospace is not
;; present)
(condition-case nil
    (set-frame-font "Hack-9")
    ;; (set-frame-font "Monospace-9")
    ;; (set-frame-font "Anonymous Pro-10")
  (error (set-frame-font "DejaVu Sans Mono-10")))

;;----------------------------------------------------------------------------
;; Configure keys
;;----------------------------------------------------------------------------
(global-unset-key (kbd "C-z")) ; Stops C-z from minimizing window
(global-set-key (kbd "M-0") (lambda () (interactive) (modify-frame-parameters nil '((alpha . 100))))) ; M-0 standard visibility
(global-set-key (kbd "s-C-+") 'sacha/increase-font-size)  ; C-+ increase font size
(global-set-key (kbd "s-C--") 'sacha/decrease-font-size)  ; C-- decrease font size
(global-set-key (kbd "<f12>") 'revert-buffer-no-confirm)
(global-set-key (kbd "s-h") 'global-hl-line-mode)         ; Highlight current line

;;---------------------------------------------------------------------------
;; FullScreen
;;---------------------------------------------------------------------------
(defun myemacs/toggle-fullscreen ()
  "Return a message string if the current doc string is invalid."
  (interactive)
  (x-send-client-message nil 0 nil "_NET_WM_STATE" 32
                         '(2 "_NET_WM_STATE_FULLSCREEN" 0)))
(global-set-key (kbd "<f11>") 'myemacs/toggle-fullscreen) ; F11 FullScreen

;;----------------------------------------------------------------------------
;; Define custom browser
;;----------------------------------------------------------------------------
(setq browse-url-browser-function 'browse-url-generic
      browse-url-generic-program "chrome")

;;----------------------------------------------------------------------------
;; sudo open file option
;;----------------------------------------------------------------------------
(defun sudo-find-file (file-name)
  "Like find file, but opens the file as root."
  (interactive "FSudo Find File: ")
  (let ((tramp-file-name (concat "/sudo::" (expand-file-name file-name))))
    (find-file tramp-file-name)))

;;----------------------------------------------------------------------------
;; clock
;;----------------------------------------------------------------------------
(setq display-time-day-and-date t)
(display-time)

;;----------------------------------------------------------------------------
;; Saravia Configure keys
;;----------------------------------------------------------------------------
(global-set-key (kbd "<f5>") 'duplicate-line)
(global-set-key (kbd "<s-f1>") 'visual-line-mode)
(global-set-key (kbd "C-x C-,") 'this-us-keyboard-to-minor-wath)
(global-set-key (kbd "C-x C-.") 'this-us-keyboard-to-more-wath)

;;----------------------------------------------------------------------------
;;; Tildes en Ubuntu
;;----------------------------------------------------------------------------
(require 'iso-transl)
;;----------------------------------------------------------------------------
;;; Algo pobre pero para quitar los tabs
;;----------------------------------------------------------------------------
(setq indent-tabs-mode nil)

;;---------------------------------------------------------------------------
;; xml beautyfer for copy and paste at M-:
;;---------------------------------------------------------------------------

;; (execute-kbd-macro (kbd "M-% > < RET > C-q C-j < RET ! C-M-\\"))

;;---------------------------------------------------------------------------
;; Jefe Mode
;;---------------------------------------------------------------------------

;; Put this in a terminal emulator
;; Usage: emacs --eval "(progn (cua-mode 1) (scroll-bar-mode 1))"

;;---------------------------------------------------------------------------
;; GNU Emacs Exwm
;;---------------------------------------------------------------------------

;; Put this in your .xinitrc
;; Usage: exec emacs --eval "(progn (require 'init-exwm) (exwm-enable) (require 'exwm-systemtray) (exwm-systemtray-enable) (set-background-color \"Black\") (scroll-bar-mode 1))"


(provide 'init-gui)

;; End:
;;; init-gui.el ends here
