;;; init-utils.el --- .Emacs Configuration -*- lexical-binding: t -*-
;;; Commentary:
;;

;;; Code:
;; Loads functions from libs
(defun load-directory (dir)
  "Load functions from the libs DIR.
read the .el files"
  (let ((load-it (lambda (f)
                   (load-file (concat (file-name-as-directory dir) f)))
		 ))
    (mapc load-it (directory-files dir nil "\\.el$"))))

;;------------------------------------------------------------------------------
;; Sachachua
;;------------------------------------------------------------------------------
;; Increase-decrease functions from Sacha Chua
(defun sacha/increase-font-size ()
  (interactive)
  (set-face-attribute 'default
                      nil
                      :height
                      (ceiling (* 1.10
                                  (face-attribute 'default :height)))))
(defun sacha/decrease-font-size ()
  (interactive)
  (set-face-attribute 'default
                      nil
                      :height
                      (floor (* 0.9
                                  (face-attribute 'default :height)))))

;; Not original from Sacha.
;; Taken from: http://emacsredux.com/blog/2013/05/22/smarter-navigation-to-the-beginning-of-a-line/
(defun sacha/smarter-move-beginning-of-line (arg)
  "Move point back to indentation of beginning of line.

Move point to the first non-whitespace character on this line.
If point is already there, move to the beginning of the line.
Effectively toggle between the first non-whitespace character and
the beginning of the line.

If ARG is not nil or 1, move forward ARG - 1 lines first.  If
point reaches the beginning or end of the buffer, stop there."
  (interactive "^p")
  (setq arg (or arg 1))

  ;; Move lines first
  (when (/= arg 1)
    (let ((line-move-visual nil))
      (forward-line (1- arg))))

  (let ((orig-point (point)))
    (back-to-indentation)
    (when (= orig-point (point))
      (move-beginning-of-line 1))))

;;-----------------------------------------------------------------------------
;; Saravia
;;-----------------------------------------------------------------------------
;; Duplicate lines key

(defun duplicate-line()
  (interactive)
  (move-beginning-of-line 1)
  (kill-line)
  (yank)
  (open-line 1)
  (next-line 1)
  (yank)
  )

(defun this-us-keyboard-to-minor-wath ()
  "Insert snippet."
  (interactive)
  (insert "<"))


(defun this-us-keyboard-to-more-wath ()
  "Insert snippet."
  (interactive)
  (insert ">"))

;; un cello de la fecha
(defun timestamp ()
  "Insert snippet."
  (interactive)
  (insert (format-time-string "agregado: %m-%d-%Y")))

;;------------------------------------------------------------------------------
(provide 'init-utils)
;;; init-utils.el ends here
