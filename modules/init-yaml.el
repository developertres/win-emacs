;;; init-yaml.el --- .Emacs Configuration -*- lexical-binding: t -*-
;;; Commentary:
;;

;;; Code:
;; yaml-mode
(use-package yaml-mode
  :mode ("\\.yml\\'" . yaml-mode))

(provide 'init-yaml)
;;; init-yaml.el ends here
