;;; init-smartparens.el --- .Emacs Configuration -*- lexical-binding: t -*-
;;; Commentary:
;;

;;; Code:
;; smartparens
(use-package smartparens
  :ensure t
  :config
  (setq sp-show-pair-from-inside nil)
  (require 'smartparens-config)
  :diminish smartparens-mode)

(provide 'init-smartparens)
;; Local Variables:
;; byte-compile-warnings: (not free-vars)
;; End:
;;; init-smartparens.el ends here
