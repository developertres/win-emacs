;;; init-editorconfig.el --- .Emacs Configuration -*- lexical-binding: t -*-
;;; Commentary:
;;

;;; Code:
;;----------------------------------------------------------------------------
;; EditorConfig
;;----------------------------------------------------------------------------
(use-package editorconfig
  :config
  (when (and (require 'editorconfig nil t)
             (fboundp 'editorconfig-mode))
    (editorconfig-mode 1)))

(provide 'init-editorconfig)

;; End:
;;; init-editorconfig.el ends here
