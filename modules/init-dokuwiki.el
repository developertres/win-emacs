;;; init-dokuwiki.el --- .Emacs Configuration -*- lexical-binding: t -*-
;;; Commentary:
;;

;;; Code:
;; dokuwiki-mode
(use-package dokuwiki-mode)

(provide 'init-dokuwiki)

;; End:
;;; init-dokuwiki.el ends here
