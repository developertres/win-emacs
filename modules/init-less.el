;;; init-less.el --- .Emacs Configuration -*- lexical-binding: t -*-
;;; Commentary:
;;

;;; Code:
;;----------------------------------------------------------------------------
;; Less Mode
;;----------------------------------------------------------------------------
(use-package less-css-mode
  :mode ("\\.less\\'" . less-css-mode))

(provide 'init-less)

;; End:
;;; init-less.el ends here
