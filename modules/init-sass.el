;;; init-sass.el --- .Emacs Configuration -*- lexical-binding: t -*-
;;; Commentary:
;;

;;; Code:
;;----------------------------------------------------------------------------
;; Sass Mode
;;----------------------------------------------------------------------------
(use-package sass-mode
    :mode ("\\.sass\\'" . sass-mode))

(provide 'init-sass)

;;; init-sass.el ends here
