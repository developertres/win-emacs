;;; init-python.el --- .Emacs Configuration -*- lexical-binding: t -*-
;;; Commentary:
;;

;;; Code:
;;----------------------------------------------------------------------------
;; Python Mode
;;----------------------------------------------------------------------------
(add-hook 'python-mode-hook
          (lambda ()
            (setq indent-tabs-mode nil)
            (setq python-indent-guess-indent-offset nil)
            (setq python-indent-offset 4)))

(use-package python
  :mode
  ("\\.py" . python-mode))

(use-package elpy
  :init
  (advice-add 'python-mode :before 'elpy-enable)
  :mode
  ("\\.py$" . python-mode)
  :config
  (setq elpy-rpc-backend "jedi")
  :bind
  (:map elpy-mode-map
        ("M-." . elpy-goto-definition)
        ("M-," . pop-tag-mark)))

(setq auto-mode-alist
      (append '(("SConstruct\\'" . python-mode)
                ("SConscript\\'" . python-mode))
              auto-mode-alist))

(use-package pip-requirements
  :hook
  (pip-requirements-mode-hook . pip-requirements-auto-complete-setup))

(use-package py-autopep8)

(use-package pyvenv)

(provide 'init-python)

;; Local Variables:
;; byte-compile-warnings: (not free-vars)
;; End:
;;; init-python.el ends here
