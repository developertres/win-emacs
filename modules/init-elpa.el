;;; init-elpa.el --- .Emacs Configuration -*- lexical-binding: t -*-
;;; Commentary:
;;

;;; Code:
;;; Find and load the correct package.el
;; =====================================================
;; use-package → https://github.com/jwiegley/use-package
;; =====================================================
(require 'package)

;; Repositories
;; ================
(setq package-archives
      '(("GNU ELPA"     . "https://elpa.gnu.org/packages/")
        ("MELPA Stable" . "https://stable.melpa.org/packages/")
        ("ORG"          . "https://orgmode.org/elpa/")
        ("MELPA"        . "https://melpa.org/packages/"))
      package-archive-priorities
      '(("MELPA Stable" . 10)
        ("GNU ELPA"     . 5)
        ("ORG"          . 3)
        ("MELPA"        . 0)))

;; =================
(package-initialize)

(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

(require 'use-package)
(setq use-package-always-ensure t)
;; =====================================================
;; End use-package
;; =====================================================

(provide 'init-elpa)
;; End:
;;; init-elpa.el ends here
