;;; init-modeline.el --- .Emacs Configuration -*- lexical-binding: t -*-
;;; Commentary:
;;

;;; Code:
;;----------------------------------------------------------------------------
;; Modeline configuration
;;----------------------------------------------------------------------------
(use-package doom-modeline
  :pin "MELPA"
  :ensure t
  :hook (after-init . doom-modeline-mode)
  )

(use-package anzu
  :config
  (custom-set-variables
   '(anzu-search-threshold 1000)
   '(anzu-replace-threshold 1000)
   '(anzu-deactivate-region t)
   '(anzu-input-idle-delay 0.1)
   '(anzu-replace-to-string-separator " => "))
  (global-anzu-mode +1)
  (set-face-attribute 'anzu-mode-line nil
                      :foreground "yellow" :weight 'bold)

  (define-key isearch-mode-map [remap isearch-query-replace]  #'anzu-isearch-query-replace)
  (define-key isearch-mode-map [remap isearch-query-replace-regexp] #'anzu-isearch-query-replace-regexp)

  ;;----------------------------------------------------------------------------
  ;; Keyboard shortcuts in Anzu Mode
  ;;----------------------------------------------------------------------------
  (global-set-key (kbd "M-%") 'anzu-query-replace)
  (global-set-key (kbd "s-<SPC>") 'anzu-query-replace))

(provide 'init-modeline)

;; End:
;;; init-modeline.el ends here
