;;; init-php.el --- .Emacs Configuration -*- lexical-binding: t -*-
;;; Commentary:
;;

;;; Code:
(use-package php-refactor-mode
  :after (php-mode)
  :mode ("\\.php\\'" . php-mode)
  :hook
  (php-mode . php-refactor-mode))

(use-package php-mode
  :ensure t)

(use-package company-php
  :ensure t
  :config
  (defun cfg:php-mode-hook ()
    (interactive)
    (require 'company-php)
    (company-mode t)
    (add-to-list 'company-backends 'company-ac-php-backend))

  (add-hook 'php-mode-hook 'cfg:php-mode-hook))

(add-hook 'php-mode-hook (lambda () (subword-mode 1)))

(provide 'init-php)
;;; init-php.el ends here
