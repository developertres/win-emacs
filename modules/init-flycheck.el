;;; init-flycheck.el --- .Emacs Configuration -*- lexical-binding: t -*-
;;; Commentary:
;;

;;; Code:
(use-package flycheck
  :config
  ;; support web-mode with PHP
  (flycheck-define-checker mix-php
    "A PHP syntax checker using the PHP command line interpreter.
    See URL `https://php.net/manual/en/features.commandline.php'."
    :command ("php" "-l" "-d" "error_reporting=E_ALL" "-d" "display_errors=1"
              "-d" "log_errors=0" source)
    :error-patterns
    ((error line-start (or "Parse" "Fatal" "syntax") " error" (any ":" ",") " "
            (message) " in " (file-name) " on line " line line-end))
    :modes (php-mode php+-mode web-mode))

  (add-to-list 'flycheck-checkers 'mix-php)
  ;; Enable for only languages
  :hook
  (c++-mode  . flycheck-mode)
  ;; (emacs-lisp-mode flycheck-mode)
  (html-mode    . flycheck-mode)
  (js-mode      . flycheck-mode)
  (python-mode  . flycheck-mode)
  (web-mode     . flycheck-mode)
  (sh-mode      . flycheck-mode))

(provide 'init-flycheck)

;; End:
;;; init-flycheck.el ends here
