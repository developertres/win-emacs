;;; init-emmet-mode.el --- .Emacs Configuration -*- lexical-binding: t -*-
;;; Commentary:
;;

;;; Code:
;; emmet-mode
(use-package emmet-mode
  ;; Enable for only languages
  :hook
  (sgml-mode . emmet-mode) ;; Auto-start on any markup modes
  (web-mode  . emmet-mode) ;; enable Emmet on web-mode
  (css-mode  . emmet-mode) ;; enable Emmet's css abbreviation.
)

(provide 'init-emmet-mode)

;; End:
;;; init-emmet-mode.el ends here
