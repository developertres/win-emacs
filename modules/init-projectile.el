;;; init-projectile.el --- .Emacs Configuration -*- lexical-binding: t -*-
;;; Commentary:
;;

;;; Code:
;; Projectile
(use-package projectile
  :ensure t
  :init
  (projectile-mode))

(provide 'init-projectile)
;;; init-projectile.el ends here
