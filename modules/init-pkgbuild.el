;;; init-pkgbuild.el --- .Emacs Configuration -*- lexical-binding: t -*-
;;; Commentary:
;;

;;; Code:
(use-package pkgbuild-mode
  :mode ("/PKGBUILD$" . pkgbuild-mode))

(provide 'init-pkgbuild)
;;; init-pkgbuild.el ends here
