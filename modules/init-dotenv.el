;;; init-dotenv.el --- .Emacs Configuration -*- lexical-binding: t -*-
;;; Commentary:
;;

;;; Code:
;; dotenv-mode
(use-package dotenv-mode
  :pin "MELPA"
  :mode ("\\.env\\'" . dotenv-mode))

(provide 'init-dotenv)
;;; init-dotenv.el ends here
