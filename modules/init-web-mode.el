;;; init-web-mode.el --- .Emacs Configuration -*- lexical-binding: t -*-
;;; Commentary:
;;

;;; Code:
(use-package web-mode
  :pin "MELPA"
  :mode (("\\.html?\\'"     . web-mode)
         ("\\.djhtml\\'"    . web-mode)
         ("\\.blade.php\\'" . web-mode)
         ("\\.tpl\\'"       . web-mode)
         ("\\.twig\\'"      . web-mode)
         ("\\.jsp\\'"       . web-mode)
         ("\\.gsp\\'"       . web-mode)
         ("\\.scss\\'"      . web-mode)
         ("\\.ctp\\'"       . web-mode))
  :config
  ;; web-modeの設定
  (setq web-mode-enable-current-element-highlight t)
  (set-face-background 'web-mode-current-element-highlight-face "#616161")

  ;; Fix smarty
  (setq web-mode-engines-alist
        '(("smarty" . "\\.tpl\\'")))

  ;; Django hook
  ;; require projectile
  (defun my-django-mode ()
    (when (and (require 'web-mode nil t)
               (fboundp 'web-mode-set-engine))
      (if (projectile-project-p)
          (when (file-exists-p (concat (projectile-project-root) "manage.py"))
            (web-mode-set-engine "django")

            ;; HTML auto functions
            (setq web-mode-enable-auto-opening t)
            (setq web-mode-enable-auto-closing t)
            (setq web-mode-enable-auto-quoting t)
            (setq web-mode-enable-auto-expanding t)

            ;; Auto-pairing
            (require 'smartparens)
            (sp-pair "{% " " %}")
            (sp-pair "{{ " " }}")
            (sp-pair "{# " " #}")
            (sp-pair "{" nil :actions :rem)
            (sp-pair "<" ">")
            (setq web-mode-enable-auto-pairing nil)))))
  ;; Pelican hook
  (defun my-pelican-mode ()
    (when (and (require 'web-mode nil t)
               (fboundp 'web-mode-set-engine))
      (if (projectile-project-p)
          (when (file-exists-p (concat (projectile-project-root) "pelicanconf.py"))
            (web-mode-set-engine "django")

            ;; HTML auto functions
            (setq web-mode-enable-auto-opening t)
            (setq web-mode-enable-auto-closing t)
            (setq web-mode-enable-auto-quoting t)
            (setq web-mode-enable-auto-expanding t)

            ;; Auto-pairing
            (require 'smartparens)
            (sp-pair "{% " " %}")
            (sp-pair "{" nil :actions :rem)
            (sp-pair "<" ">")
            (setq web-mode-enable-auto-pairing nil)))))

  ;; Saravia
  (setq standard-indent 2)
  (setq-default indent-tabs-mode nil)
  (electric-pair-local-mode -1)
  (setq web-mode-enable-auto-indentation nil)
  ;; para iluminar mejor la sintaxis, para mas ver
  ;; web-mode.org highlingth variables
  (setq web-mode-enable-current-element-highlight t)
  (setq web-mode-enable-current-column-highlight nil)

  :hook
  (web-mode . my-django-mode)
  (web-mode . my-pelican-mode))

(provide 'init-web-mode)
;;; init-web-mode.el ends here
