;;; init-diminish.el --- .Emacs Configuration -*- lexical-binding: t -*-
;;; Commentary:
;;

;;; Code:
;;----------------------------------------------------------------------------
;; Diminish - is minor modes with no modeline display
;;----------------------------------------------------------------------------
(use-package diminish)

;; Hide undo-tree-mode
(diminish 'undo-tree-mode)

(provide 'init-diminish)

;; End:
;;; init-diminish.el ends here
