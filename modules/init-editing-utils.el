;;; init-editing-utils.el --- .Emacs Configuration -*- lexical-binding: t -*-
;;; Commentary:
;;

;;; Code:

;;----------------------------------------------------------------------------
;; Remove whitespaces
;;----------------------------------------------------------------------------
;; Delete trailing whitespace before saving fil → all modes
;; (add-hook 'before-save-hook 'delete-trailing-whitespace)

;; Delete-trailing-whitespace-when-saving-except-certain-modes
(add-hook 'before-save-hook
          (lambda ()
            (unless (eq major-mode 'diff-mode)
              (delete-trailing-whitespace))))
;;----------------------------------------------------------------------------
;; Automatic pairs open symbols (, {, [...
;; Disable for default.
;; Uncomment the next 4 lines if you want to enable the pairs-mode
;;(when (fboundp 'electric-pair-mode)
;;  (electric-pair-mode))
;;(when (eval-when-compile (version< "24.4" emacs-version))
;;  (electric-indent-mode 1))

;; Active auto-revert-mode that automatically reloads modified files out of Emacs.
;; It is very useful to see logs (like auto-revert-tail-mode) among many other cases.
(global-auto-revert-mode)
(setq global-auto-revert-non-file-buffers t
      auto-revert-verbose nil)

;;----------------------------------------------------------------------------
;; Show matching parens
;;----------------------------------------------------------------------------
(show-paren-mode 1)

;;----------------------------------------------------------------------------
;; More useful things - only one line
;;----------------------------------------------------------------------------
(set-default 'truncate-lines t)
(setq show-trailing-whitespace nil)

;;---------------------------------------------------------------------------
;; XML
;;--------------------------------------------------------------------------
(setq nxml-child-indent 4 nxml-attribute-indent 4)

;;----------------------------------------------------------------------------
;; Some basic preferences
;;----------------------------------------------------------------------------
(setq-default
 indent-tabs-mode nil)

;;----------------------------------------------------------------------------
;;Fix ORG
;;----------------------------------------------------------------------------
(setq org-support-shift-select 'always)
;;----------------------------------------------------------------------------
;; Undo-tree
;;----------------------------------------------------------------------------
(use-package undo-tree
  :demand t
  :diminish undo-tree-mode
  :bind (("C-x u" . undo-tree-visualizer)
         (:map
          undo-tree-visualizer-mode-map ("RET" . undo-tree-visualizer-quit)))
  :init
  (defadvice undo-tree-make-history-save-file-name
      (after undo-tree activate)
    (setq ad-return-value (concat ad-return-value ".7z")))

  (defadvice undo-tree-visualize (around undo-tree-split-side-by-side activate)
    "Divide la ventana de lado a lado al visualizar undo-tree-visualize"
    (let ((split-height-threshold nil)
          (split-width-threshold 0))
      ad-do-it))

  (setf undo-tree-visualizer-timestamps t)
  (setf undo-tree-visualizer-diff nil)
  (setf undo-tree-auto-save-history nil) ;; no salva el historial de cambios

  :config
  (defalias 'redo 'undo-tree-redo)
  (global-undo-tree-mode 1))

;;----------------------------------------------------------------------------
;; which-key
;;----------------------------------------------------------------------------
(use-package which-key
  :pin "MELPA"
  :ensure t
  :init
  (which-key-mode))

(provide 'init-editing-utils)

;; Local Variables:
;; byte-compile-warnings: (not free-vars)
;; End:
;;; init-editing-utils.el ends here
