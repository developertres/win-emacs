;;; init-scss.el --- .Emacs Configuration -*- lexical-binding: t -*-
;;; Commentary:
;;

;;; Code:
;;----------------------------------------------------------------------------
;; Scss Mode
;;----------------------------------------------------------------------------
;; (use-package scss-mode
;;   :defer t
;;   :mode ("\\.scss\\'" . scss-mode)
;;   :config
;;   (setq scss-compile-at-save 'nil))

(provide 'init-scss)

;;; init-scss.el ends here
