;;; init-nginx.el --- .Emacs Configuration -*- lexical-binding: t -*-
;;; Commentary:
;;

;;; Code:
;;---------------------------
;; Nginx Mode
;;---------------------------
(use-package nginx-mode
  :mode ("/nginx/sites-\\(?:available\\|enabled\\)/" . nginx-mode))

(provide 'init-nginx)

;; End:
;;; init-nginx.el ends here
