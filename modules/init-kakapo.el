;;; init-kakapo.el --- .Emacs Configuration -*- lexical-binding: t -*-
;;; Commentary:
;;

;;; Code:
(use-package kakapo-mode
  :ensure t
  :config
  (kakapo-mode t)
  (setq tab-width 4)

  :hook
  (web-mode . kakapo-mode)
  )

(provide 'init-kakapo)

;; End:
;;; init-kakapo.el ends here
