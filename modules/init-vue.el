;;; init-vue.el --- .Emacs Configuration -*- lexical-binding: t -*-
;;; Commentary:
;;

;;; Code:
;; vue-mode
(use-package vue-mode
  :pin "MELPA"
  :mode ("\\.vue\\'" . vue-mode)
  :config
  (setq vue-mode-packages '(vue-mode))
  (setq vue-mode-excluded-packages '())
  ;; 0, 1, or 2, representing (respectively) none, low, and high coloring
  (setq mmm-submode-decoration-level 0)
  )

(provide 'init-vue)
;;; init-vue.el ends here
