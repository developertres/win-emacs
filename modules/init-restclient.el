;;; init-restclient-mode.el --- .Emacs Configuration -*- lexical-binding: t -*-
;;; Commentary:
;;

;;; Code:
(use-package restclient
  :mode (("\\.restclient?\\'" . restclient-mode)))
(provide 'init-restclient)
;;; init-restclient-mode.el ends here
