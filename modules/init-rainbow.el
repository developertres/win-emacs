;;; init-rainbow.el --- .Emacs Configuration -*- lexical-binding: t -*-
;;; Commentary:
;;

;;; Code:
;; rainbow-mode
(use-package rainbow-mode
  :hook
  (css-mode  . rainbow-mode)
  (sass-mode . rainbow-mode)
  (scss-mode . rainbow-mode)
  (less-mode . rainbow-mode)
  (web-mode  . rainbow-mode)
  (html-mode . rainbow-mode))

(provide 'init-rainbow)
;;; init-rainbow.el ends here
