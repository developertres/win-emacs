;;; init-indent-guides.el --- .Emacs Configuration -*- lexical-binding: t -*-
;;; Commentary:
;;

;;; Code:

;; This minor mode highlights indentation levels via font-lock

(use-package highlight-indent-guides
  :config
  (setq highlight-indent-guides-method 'character)
  ;; Indent character samples: fill, column or character
  (setq highlight-indent-guides-method 'character))

(provide 'init-indent-guides)

;; End:
;;; init-indent-guides.el ends here
