;;; init-crystal.el --- .Emacs Configuration -*- lexical-binding: t -*-
;;; Commentary:
;;

;;; Code:

;;----------------------------------------------------------------------------
;; Crystal Mode
;;----------------------------------------------------------------------------
(use-package crystal-mode
  :mode ("\\.cr\\'" . crystal-mode))

(provide 'init-crystal)

;; End:
;;; init-crystal.el ends here
