;;; init-apache.el --- .Emacs Configuration -*- lexical-binding: t -*-
;;; Commentary:
;;

;;; Code:
;;---------------------------
;; Apache Mode
;;---------------------------
(use-package apache-mode
  :mode
  ("\\.htaccess\\'"   . apache-mode)
  ("httpd\\.conf\\'"  . apache-mode)
  ("srm\\.conf\\'"    . apache-mode)
  ("access\\.conf\\'" . apache-mode)
  ("sites-\\(available\\|enabled\\)/" . apache-mode))

(provide 'init-apache)

;; End:
;;; init-apache.el ends here
