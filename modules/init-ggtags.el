;;; init-ggtags.el --- .Emacs Configuration -*- lexical-binding: t -*-
;;; Commentary:
;;

    ;;; Code:
(use-package ggtags
  :ensure t
  :config
  (ggtags-mode t)

  :hook
  (c++-mode  . ggtags-mode)
  (html-mode . ggtags-mode)
  (js-mode   . ggtags-mode)
  (web-mode  . ggtags-mode)
  (sh-mode   . ggtags-mode)
  (php-mode  . ggtags-mode))


(provide 'init-ggtags)

;; End:
    ;;; init-ggtags.el ends here
