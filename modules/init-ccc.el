;;; init-ccc.el --- .Emacs Configuration -*- lexical-binding: t -*-
;;; Commentary:
;;

;;; Code:

;;----------------------------------------------------------------------------
;; c Mode
;;----------------------------------------------------------------------------
(add-hook 'c-mode-hook
          (lambda ()
            (setq indent-tabs-mode t)
            (setq c-basic-offset 4)
            (setq tab-width 4)))

;;----------------------------------------------------------------------------
;; c++ Mode
;;----------------------------------------------------------------------------
(add-hook 'c++-mode-hook
          (lambda ()
            (setq indent-tabs-mode t)
            (setq c-basic-offset 4)
            (setq tab-width 4)))

;; This is my default indent.
;; After moving the cursor to line 2 I see that the relevant symbol is comment-intro.
(c-set-offset 'comment-intro 6)

(provide 'init-ccc)

;; Local Variables:
;; byte-compile-warnings: (not free-vars)
;; End:
;;; init-ccc.el ends here
