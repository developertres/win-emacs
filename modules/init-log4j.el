;;; init-log4j.el --- .Emacs Configuration -*- lexical-binding: t -*-
;;; Commentary:
;;

;;; Code:
(use-package log4j-mode
  :ensure t
  :defer t
  :mode ("\\.log\\'" . log4j-mode))

(provide 'init-log4j)

;; Local Variables:
;; byte-compile-warnings: (not free-vars)
;; End:
;;; init-log4j.el ends here
