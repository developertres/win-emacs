;;; init-security.el --- .Emacs Configuration -*- lexical-binding: t -*-
;;; Commentary:
;;

;;; Code:
;;---------------------------------------------------------------------------------------------
;; Security. Check https://web.archive.org/web/20170413150436/https://ogbe.net/emacsconfig.html
;;---------------------------------------------------------------------------------------------
(setq tls-checktrust t)
(setq gnutls-verify-error t)
(let ((trustfile "/etc/ssl/cert.pem"))
  (setq tls-program
        `(,(format  "gnutls-cli --x509cafile %s -p %%p %%h" trustfile)
          ,(format "openssl s_client -connect %%h:%%p -CAfile %s -no_ssl2 -ign_eof" trustfile)))
  (setq gnutls-trustfiles (list trustfile)))

(provide 'init-security)

;; Local Variables:
;; byte-compile-warnings: (not free-vars)
;; End:
;;; init-security.el ends here
