;;; init-whitespace.el --- .Emacs Configuration -*- lexical-binding: t -*-
;;; Commentary:
;;

;;; Code:
;;----------------------------------------------------------------------------
;; Whitespace Mode | tool
;;----------------------------------------------------------------------------
(use-package whitespace-cleanup-mode)

(provide 'init-whitespace)
;;; init-whitespace.el ends here
