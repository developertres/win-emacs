;;; init-icons.el --- .Emacs Configuration -*- lexical-binding: t -*-
;;; Commentary:
;;

;;; Code:

;;----------------------------------
;; All-the-icons
;;----------------------------------
(use-package all-the-icons
  :ensure t
  :config
  ;;----------------------------------
  ;; Fonts - connecting with neotree
  ;;---------------------------------
  (if (member "all-the-icons" (font-family-list))
      (message "The all-the-icons are installed (.❛ ᴗ ❛.)")
    (setq neotree-mode-hook 'all-the-icons-install-fonts)))

;; fix performace
(setq inhibit-compacting-font-caches t)

(provide 'init-icons)

;; Local Variables:
;; byte-compile-warnings: (not free-vars)
;; End:
;;; init-icons.el ends here
