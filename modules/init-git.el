;;; init-git.el --- .Emacs Configuration -*- lexical-binding: t -*-
;;; Commentary:
;;

;;; Code:
(setq vc-follows-symlinks t
      find-file-visit-truename t
      vc-handled-backends nil)

(if (display-graphic-p)
    (use-package git-gutter-fringe
      :ensure t
      :config
      (global-git-gutter-mode t)
      (setq-default fringes-outside-margins nil)
      (setq-default left-fringe-width 10)
      (set-face-foreground 'git-gutter-fr:modified "purple")
      (set-face-foreground 'git-gutter-fr:added "green")
      (set-face-foreground 'git-gutter-fr:deleted "red")

      (defun my-reshape-git-gutter (gutter)
        "Re-shape gutter for `ivy-read'."
        (let* ((linenum-start (aref gutter 3))
               (linenum-end (aref gutter 4))
               (target-line "")
               (target-linenum 1)
               (tmp-line "")
               (max-line-length 0))
          (save-excursion
            (while (<= linenum-start linenum-end)
              (with-no-warnings
                (goto-line linenum-start))
              (setq tmp-line (replace-regexp-in-string "^[ \t]*" ""
                                                       (buffer-substring (line-beginning-position)
                                                                         (line-end-position))))
              (when (> (length tmp-line) max-line-length)
                (setq target-linenum linenum-start)
                (setq target-line tmp-line)
                (setq max-line-length (length tmp-line)))

              (setq linenum-start (1+ linenum-start))))
          ;; build (key . linenum-start)
          (cons (format "%s %d: %s"
                        (if (eq 'deleted (aref gutter 1)) "-" "+")
                        target-linenum target-line)
                target-linenum)))

      (defun my-goto-git-gutter ()
        (interactive)
        (eval-when-compile (require 'git-gutter-fringe nil t))
        (when (and (require 'git-gutter-fringe nil t)
                   (fboundp 'ivy-read))

          (if git-gutter-fr:diffinfos
              (ivy-read "git-gutters-fr:"
                        (mapcar 'my-reshape-git-gutter git-gutter-fr:diffinfos)
                        :action (lambda (e)
                                  ;; ivy9+ keep `(car e)'
                                  ;; ivy8- strip the `(car e)'
                                  ;; we handle both data structure
                                  (unless (numberp e) (setq e (cdr e)))
                                  (with-no-warnings
                                    (goto-line e))))
            (message "NO git-gutters-fringe!"))
          ))))

(use-package gitconfig-mode
  :ensure t
  :mode ("/\\.?git/?config$"
         "/\\.gitmodules$")
  :hook
  (gitconfig-mode . flyspell-mode))

(use-package gitignore-mode
  :ensure t
  :mode ("/\\.gitignore$"
         "/\\.git/info/exclude$"
         "/git/ignore$"))

(use-package gitattributes-mode
  :ensure t
  :defer t)

(use-package git-timemachine
  :ensure t
  :commands git-timemachine
  :bind (:map git-timemachine-mode
              ("c" . git-timemachine-show-current-revision)
              ("b" . git-timemachine-switch-branch)))

;;; smerge-mode video explain https://emacsgifs.github.io/public/videos/758861381898637313.mp4
(use-package smerge-mode
  :ensure t
  :config
  (defun enable-smerge-maybe ()
    (when (and buffer-file-name (vc-backend buffer-file-name))
      (save-excursion
        (goto-char (point-min))
        (when (re-search-forward "^<<<<<<< " nil t)
          (smerge-mode +1)))))

  (eval-when-compile (require 'smerge-mode nil t))
  (when (and (require 'smerge-mode nil t)
             (fboundp 'enable-smerge-maybe))
    (add-hook 'buffer-list-update-hook #'enable-smerge-maybe)))

(provide 'init-git)

;; Local Variables:
;; byte-compile-warnings: (not free-vars)
;; End:
;;; init-git.el ends here
