;;; init-html.el --- .Emacs Configuration -*- lexical-binding: t -*-
;;; Commentary:
;;

;;; Code:

;;;;;;;;;;;;;;;;;;;;;;;;;
;; HTML Mode
;;;;;;;;;;;;;;;;;;;;;;;;;

(add-hook 'html-mode-hook
          (lambda ()
            ;; Default indentation is usually 2 spaces, changing to 4.
            (set (make-local-variable 'sgml-basic-offset) 4)))

;; highlight-indent-guides-mode
(add-hook 'html-mode-hook 'highlight-indent-guides-mode)

(provide 'init-html)

;; Local Variables:
;; byte-compile-warnings: (not free-vars)
;; End:
;;; init-html.el ends here
