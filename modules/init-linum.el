;;; init-nlinum.el --- .Emacs Configuration -*- lexical-binding: t -*-
;;; Commentary:
;;

;;; Code:
;;----------------------------------------------------------------------------
;; Line numbers
;;----------------------------------------------------------------------------
;; Linum snippets from: https://www.emacswiki.org/emacs/LineNumbers

(use-package linum
  :config
  (defun my-linum-get-format ()
    "Defines LINUM-GET-FORMAT"
    (let* ((width (1+ (length (number-to-string
                               (count-lines (point-min) (point-max))))))
           (format (concat "%" (number-to-string width) "d \u2502")))
      (setq my-linum-format-string format)))
  (add-hook 'linum-before-numbering-hook 'my-linum-get-format)

  (defun my-linum-format (line-number)
    "Defines LINE-FORMAT"
    (propertize (format my-linum-format-string line-number) 'face
                (if (eq line-number my-current-line)
                    'my-linum-hl
                  'linum)))
  (setq linum-format 'my-linum-format)

  (defadvice linum-update (around my-linum-update)
    "Defines LINUM-UPDATE for update lines"
    (let ((my-current-line (line-number-at-pos)))
      ad-do-it))
  (ad-activate 'linum-update)

  ;; Colors line active
  ;; set in file init-theme.el
  ;; for example:
  ;; (defface my-linum-hl
  ;;     '((t :background "gray20" :foreground "gold"))
  ;;     "Face for the currently active Line number"
  ;;     :group 'linum)
  ;;   )

  :bind
  (([f6] . linum-mode)))

(provide 'init-linum)

;; Local Variables:
;; byte-compile-warnings: (not free-vars)
;; End:
;;; init-linum.el ends here
