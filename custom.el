(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(anzu-deactivate-region t)
 '(anzu-input-idle-delay 0.1)
 '(anzu-replace-threshold 1000)
 '(anzu-replace-to-string-separator " => ")
 '(anzu-search-threshold 1000)
 '(package-selected-packages
   (quote
    (csharp-mode restclient git-gutter-fringe yaml-mode whitespace-cleanup-mode which-key web-mode vue-mode use-package undo-tree smartparens sass-mode rjsx-mode rainbow-mode py-autopep8 projectile pkgbuild-mode pip-requirements php-refactor-mode nlinum nginx-mode neotree markdown-mode log4j-mode less-css-mode json-mode js2-refactor ivy-rich ivy-hydra highlight-indent-guides gitignore-mode gitconfig-mode gitattributes-mode git-timemachine flycheck eslintd-fix emmet-mode elpy dotenv-mode doom-themes doom-modeline dokuwiki-mode diminish crystal-mode counsel company-quickhelp company-php apache-mode anzu))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
