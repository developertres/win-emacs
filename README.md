<h1 align="center">emacs-personal</h1>
Lightweight configuration of emacs with basic utilities plus personal configuration

![emacs-personal](https://archive.org/download/libreweb/emacs-personal-1.0.png?raw=true "emacs-personal")

### Installation

First step:

    M-x package-refresh-contents [RET]

#### Main mirror

1. `git clone https://notabug.org/saravia/emacs-personal.git ~/.emacs.d/`

2. `emacs --batch --eval='(load-file "~/.emacs.d/init.el")'`

### Fonts

`pacman -S ttf-hack` (for default) or paste to ~/.local/share/fonts/ this https://github.com/source-foundry/Hack/releases/download/v3.003/Hack-v3.003-ttf.zip

`pacman -S ttf-anonymous-pro`

> change the typography in the [init-gui.el](modules/init-gui.el#L54) file

### Markdown Mode
- Require installed markdown in distro GNU+Linux, example:

  `apt install markdown` or `pacman -S markdown`

### Flycheck Mode
- Require installed the languages checking, example:

  `pacman -S shellcheck`    # bash, sh

  `pacman -S python-pylint` # python

### Neotree

Neotree comes by default in combination with all-the-icons.
So pressing `[f8]` will display (you must give "yes") a one-time screen to install all-the-icons fonts.

### Linum
So pressing `[f6]` will display

### Mode Python
- Require installed virtualenv:

        pacman -S python-virtualenv

- Enable or disable pyvenv-mode: <kdb>M-x</kdb> pyvenv-mode

- To create virtualenv from emacs:

        pyvenv-create

- To activate virtualenv from emacs:

        pyvenv-activate

## Troubleshooting

```bash
Failed to verify signature archive-contents.sig:
No public key for 066DAFCB81E42C40 created at 2019-10-05T04:10:02-0500 using RSA
Command output:
gpg: Firmado el sáb 05 oct 2019 04:10:02 -05
gpg:                usando RSA clave C433554766D3DDC64221BFAA066DAFCB81E42C40
gpg: Imposible comprobar la firma: No public key
```

Fixed with:

    gpg --homedir ~/.emacs.d/elpa/gnupg --receive-keys 066DAFCB81E42C40

## EXWM (Emacs X Window Manager)

Poner en el .xinitrc:

    exec emacs --eval "(progn (require 'init-exwm) (exwm-enable) (require 'exwm-systemtray) (exwm-systemtray-enable) (set-background-color \"Black\") (scroll-bar-mode 1))"

Opcion 2, poner en el `/usr/share/xsessions/exwm.desktop`:

    [Desktop Entry]
    Name=EXWM
    Comment=Gracias heckyel por escribit emacs personal modular
    Exec=emacs --eval "(progn (require 'init-exwm) (exwm-enable) (require 'exwm-systemtray) (exwm-systemtray-enable) (set-background-color \"Black\") (scroll-bar-mode 1))"
    Type=Application
