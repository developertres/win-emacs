;;; init.el --- .Emacs Configuration -*- lexical-binding: t -*-
;;; Commentary:
;;
;; Emacs!!!

;;; Code:
;; Without this comment emacs25 adds (package-initialize) here
;; (package-initialize)

;;; Time Mark
(setq emacs-load-start-time (current-time))

;;; Welcome message
(setq-default initial-scratch-message
              (concat ";; Happy hacking, " user-login-name " - Emacs loves you!\n\n"))

;;; Modules directory
(push (concat user-emacs-directory "modules") load-path)

;;;------------------------------
;;; Features
;;;------------------------------
;;; https://stackoverflow.com/questions/2816019/in-lisp-avoid-cannot-open-load-file-when-using-require

;; @see https://www.reddit.com/r/emacs/comments/3kqt6e/2_easy_little_known_steps_to_speed_up_emacs_start/
;; Normally file-name-handler-alist is set to
;; (("\\`/[^/]*\\'" . tramp-completion-file-name-handler)
;; ("\\`/[^/|:][^/|]*:" . tramp-file-name-handler)
;; ("\\`/:" . file-name-non-special))
;; Which means on every .el and .elc file loaded during start up, it has to runs those regexps against the filename.
(let* ((file-name-handler-alist nil))
  ;; package-initialize' takes 35% of startup time
  ;; need check https://github.com/hlissner/doom-emacs/wiki/FAQ#how-is-dooms-startup-so-fast for solution
  (require 'init-security)
  (require 'init-elpa)
  ;; theme
  (require 'init-theme)
  ;; Utils
  (require 'init-utils)
  ;; GUI
  (require 'init-linum)
  (require 'init-gui)
  (require 'init-editing-utils)
  (require 'init-diminish)
  (require 'init-modeline)
  (require 'init-indent-guides)
  (require 'init-icons)
  (require 'init-neotree)
  ;; Tools
  (require 'init-apache)
  (require 'init-company)
  (require 'init-flycheck)
  ;; (require 'init-ivy)
  (require 'init-log4j)
  (require 'init-whitespace)
  (require 'init-emmet-mode)
  (require 'init-nginx)
  (require 'init-git)
  ;;(require 'init-editorconfig nil 'noerror)
  ;; Languages
  (require 'init-ccc)
  (require 'init-crystal)
  (require 'init-html)
  (require 'init-js-two)
  (require 'init-markdown)
  (require 'init-php)
  (require 'init-projectile)
  (require 'init-python)
  (require 'init-pkgbuild)
  (require 'init-less)
  (require 'init-smartparens)
  (require 'init-sass)
  (require 'init-scss)
  (require 'init-vue)
  (require 'init-yaml)
  ;; Plus
  (require 'init-rainbow)
  (require 'init-web-mode)
  (require 'init-dotenv)
  (require 'init-dokuwiki)

  (require 'init-ggtags)
  (require 'init-kakapo)
  (require 'init-multiple-cursors)
  (require 'init-restclient))

;;; Custom variables
(setq custom-file (expand-file-name "custom.el" user-emacs-directory))
;;; Loads custom file
(when (file-exists-p custom-file)
  (load custom-file))

;;; Settings
(setq settings-file (expand-file-name "settings.el" user-emacs-directory))
;;; Loads settings file
(when (file-exists-p custom-file)
  (load settings-file))

;; Time Output
(when window-system
  (let ((elapsed (float-time (time-subtract (current-time)
                                            emacs-load-start-time))))
    (message "[STARTUP] Loading %s ... done (%.3fs)" load-file-name elapsed)))

;; enable erase-buffer command
(put 'erase-buffer 'disabled nil)

;; set up unicode
;; keyboard / input method settings
(setq locale-coding-system 'utf-8)
(set-terminal-coding-system 'utf-8)
(set-keyboard-coding-system 'utf-8)
(set-selection-coding-system 'utf-8)
(set-language-environment 'UTF-8) ; prefer utf-8 for language settings
(set-default-coding-systems 'utf-8)
(setq default-buffer-file-coding-system 'utf-8)
(setq x-select-request-type '(UTF8_STRING COMPOUND_TEXT TEXT STRING))
(prefer-coding-system       'utf-8)
(setq buffer-file-coding-system 'utf-8-unix)
(setq default-file-name-coding-system 'utf-8-unix)
(setq default-keyboard-coding-system 'utf-8-unix)
(setq default-process-coding-system '(utf-8-unix . utf-8-unix))
(setq default-sendmail-coding-system 'utf-8-unix)
(setq default-terminal-coding-system 'utf-8-unix)



(provide 'init)
;; Local Variables:
;; byte-compile-warnings: (not free-vars)
;; End:
;;; init.el ends here
